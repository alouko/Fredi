/****************************************************************************
    Copyright (C) 2006,2012 Stefan Bormann, Martin Pischky

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*****************************************************************************

    IMPORTANT:

    Note: The sale any LocoNet device hardware (including bare PCB's) that
    uses this or any other LocoNet software, requires testing and certification
    by Digitrax Inc. and will be subject to a licensing agreement.

    Please contact Digitrax Inc. for details.

*****************************************************************************

 Title :   Application module for bootloader usage
 Author:   Stefan Bormann <stefan.bormann@gmx.de>
           Martin Pischky <martin@pischky.de>
 Date:     17-Mar-2006, 27-Apr-2012
 Software: AVR-GCC
 Target:   AtMega

 DESCRIPTION
       Implementation of LocoNet message parser for setup message of
       IPL protocol. This module makes an application updateable,
       because it allows to enter the downloader software in the
       bootloader from the running application like this:

       if (BootloaderParseMessage(&stLnMsg) == 1)
       {
           BootloaderEnter();
       }

*****************************************************************************/

#ifndef _BOOTLOADER_USER_H_
#define _BOOTLOADER_USER_H_

#include <stdint.h>          // typedef int8_t
#include <avr/pgmspace.h>
#include <avr/interrupt.h>   // #define cli()
#include "loconet.h"         // Opcodes and message union


#ifndef BOOTSTRAP_START_BYTE
#error "please define BOOTSTRAP_START_BYTE with command line"
#endif


// Return the software version of the bootstrap loader
static unsigned char inline BootloaderGetSoftwareVersion(void)
{
	return pgm_read_byte_near(FLASHEND);
}


// Return the hardware version as far as the bootloader knows
static unsigned char inline BootloaderGetHardwareVersion(void)
{
	return pgm_read_byte_near(FLASHEND-1);
}



#define JUMP_TO_ABSOLUTE(word_addr) __asm__ __volatile__ ("ijmp"::"z"(word_addr))



// Jump into bootloader to initiate update. 
// Should be done after recognition of a matching IPL setup message.
static void inline BootloaderEnter(void)
{
	cli(); // Disable all interrupts: bootloader should run without interrupts.
	       // Testing shows that they are disabled without this, but the manual
	       // does not explain why. So we do this paranoid cli.

// inform bootloader that this is a call and not a boot
#if defined(__AVR_ATmega8__)
	MCUCSR = 0;
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega168__) || defined(__AVR_ATmega328P__)
	MCUSR = 0;  // wer hat dem Erfinder dieser Umbenamsung ins Gehirn geschissen????
#else
	#error "don't know how the MCUcSR is called on this processor"
#endif

	JUMP_TO_ABSOLUTE(BOOTSTRAP_START_BYTE / 2);
}


// Parse incoming LocoNet message and check for a matching IPL setup message.
// Possible return values:
// +1  matching IPL setup message received, should jump to bootloader now
//  0  this is not an IPL setup message
// <0  IPL setup message is not matching
int8_t BootloaderParseMessage(lnMsg *pstMsg);


#endif // duplicate inclusion
