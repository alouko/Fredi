/****************************************************************************
    Copyright (C) 2006 Stefan Bormann

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*****************************************************************************

    IMPORTANT:

    Note: The sale any LocoNet device hardware (including bare PCB's) that
    uses this or any other LocoNet software, requires testing and certification
    by Digitrax Inc. and will be subject to a licensing agreement.

    Please contact Digitrax Inc. for details.

*****************************************************************************

 Title :   Application module for bootloader usage
 Author:   Stefan Bormann <stefan.bormann@gmx.de>
           Martin Pischky <martin@pischky.de>
 Date:     17-Mar-2006, 5-Feb-2012
 Software: AVR-GCC
 Target:   AtMega

 $Id: BootloaderUser.c,v 1.6 2012/02/06 16:31:07 pischky Exp $

 DESCRIPTION
       Implementation of LocoNet message parser for setup message of
       IPL protocol. This module makes an application updateable.

*****************************************************************************/

#include "BootloaderUser.h"  // Prototypes of this module
#include "utils.h"           // decodePeerData()

#undef DEBUG_BOOTLOADERUSER

#ifdef DEBUG_BOOTLOADERUSER
	#include "vprintf.h"     // _printf_P(),
#endif

#define INDEX_MFG        0
#define INDEX_PROD_LOW   1
#define INDEX_HW_VERSION 2
#define INDEX_SW_VERSION 3
#define INDEX_FLAGS      4
#define INDEX_DEVELOPER  6
#define INDEX_PROD_HIGH  7

// Flags (sometimes called "options")
#define DO_NOT_CHECK_SOFTWARE_VERSION        0x00
#define CHECK_SOFTWARE_VERSION_LESS          0x04

#define DO_NOT_CHECK_HARDWARE_VERSION        0x00
#define REQUIRE_HARDWARE_VERSION_EXACT_MATCH 0x01
#define ACCEPT_LATER_HARDWARE_VERSIONS       0x03

#define SW_FLAGS_MSK                         0x04
#define HW_FLAGS_MSK                         0x03

// 0xE5 0x10 0xFF 0xFF 0xFF PXCT1 D1 D2 D3 D4 PXCT2 D5 D6 D7 D8 
int8_t BootloaderParseMessage(lnMsg *pstMsg)
{
	uint8_t aucData[8];
	register uint8_t ucFlags;

	if ( pstMsg->px.command     != OPC_PEER_XFER) return 0;
	if ( pstMsg->px.mesg_size   != 0x10) return 0;
	if ( pstMsg->px.src         != 0x7F) return 0;
	if ( pstMsg->px.dst_l       != 0x7F) return 0;
	if ( pstMsg->px.dst_h       != 0x7F) return 0;
	if ((pstMsg->px.pxct1&0xF0) != 0x40) return 0;
	if ((pstMsg->px.pxct2&0xF0) != 0x00) return 0; // "setup" IPL message type is 0

	decodePeerData(&pstMsg->px, aucData);

	#ifdef DEBUG_BOOTLOADERUSER
		_printf_P(PSTR("aucData= %x %x %x %x %x %x %x %x\r\n"),
				aucData[INDEX_MFG], aucData[INDEX_PROD_LOW],
				aucData[INDEX_HW_VERSION], aucData[INDEX_SW_VERSION],
				aucData[INDEX_FLAGS], aucData[5],
				aucData[INDEX_DEVELOPER], aucData[INDEX_PROD_HIGH]);
	#endif

	if (aucData[INDEX_MFG]       != MANUFACTURER_ID) return -1;
	if (aucData[INDEX_DEVELOPER] != DEVELOPER_ID)    return -2;
	if (aucData[INDEX_PROD_LOW]  != (unsigned char)(PRODUCT_ID&0x00ff)) return -3;
	if (aucData[INDEX_PROD_HIGH] != (unsigned char)(PRODUCT_ID>>8))     return -3;

	ucFlags = aucData[INDEX_FLAGS];
	#ifdef DEBUG_BOOTLOADERUSER
		_printf_P(PSTR("ucFlags=0x%x\r\n "), ucFlags);
	#endif

	if ((ucFlags & SW_FLAGS_MSK) != DO_NOT_CHECK_SOFTWARE_VERSION)
	{
		#ifdef DEBUG_BOOTLOADERUSER
			_printf_P(PSTR("checking SW version: "
					"aucData[INDEX_SW_VERSION]=%d "
					"SOFTWARE_VERSION=%d ret=%d\r\n"),
					aucData[INDEX_SW_VERSION], SOFTWARE_VERSION,
					(aucData[INDEX_SW_VERSION] <= SOFTWARE_VERSION));
		#endif
		if (aucData[INDEX_SW_VERSION] <= SOFTWARE_VERSION)
		{
			return -4;
		}
	}

	#ifdef HARDWARE_VERSION // does this project uses HW versions?
		#if ! defined(HARDWARE_VERS_TEST)
			#error Please define HARDWARE_VERS_TEST in your makefile and pass to compiler with -D
		#elif (HARDWARE_VERS_TEST != 1) && (HARDWARE_VERS_TEST != 2)
			#error HARDWARE_VERS_TEST should be defined as '1' or '2'
		#else
			#if HARDWARE_VERS_TEST == 1
				uint8_t hwFlags = ucFlags & HW_FLAGS_MSK;
				if (hwFlags != DO_NOT_CHECK_HARDWARE_VERSION)
				{
					#ifdef DEBUG_BOOTLOADERUSER
						_printf_P(PSTR("checking HW version: "
								"aucData[INDEX_HW_VERSION]=%d "
								"HARDWARE_VERSION=%d ucFlags=%d\r\n"),
								aucData[INDEX_HW_VERSION], HARDWARE_VERSION,
								ucFlags);
					#endif
					if (hwFlags == REQUIRE_HARDWARE_VERSION_EXACT_MATCH)
					{
						if (aucData[INDEX_HW_VERSION] != HARDWARE_VERSION)
						{
							return -5;
						}
					}
					else if (hwFlags == ACCEPT_LATER_HARDWARE_VERSIONS)
					{
						if (aucData[INDEX_HW_VERSION] > HARDWARE_VERSION)
						{
							return -6;
						}
					}
				}
			#elif HARDWARE_VERS_TEST == 2
				#warning compiling with forced exact hardware version match
				#ifdef DEBUG_BOOTLOADERUSER
					_printf_P(PSTR("checking exact HW version: "
							"aucData[INDEX_HW_VERSION]=%d "
							"HARDWARE_VERSION=%d ret=%d\r\n"),
							aucData[INDEX_HW_VERSION], HARDWARE_VERSION,
							(aucData[INDEX_HW_VERSION] != HARDWARE_VERSION));
				#endif
				if (aucData[INDEX_HW_VERSION] != HARDWARE_VERSION)
				{
					// don't accept other hw version
					return -5;
				}
			#endif
		#endif
	#endif //#ifdef HARDWARE_VERSION

	return 1;  // received matching setup message, should jump to bootloader now
}




