#*******************************************************************************
#    Copyright (C) 2011, 2012 Martin Pischky Stefan Bormann
#*******************************************************************************
# include for main project makefile
# $Id: bootldr_checksum.mk,v 1.9 2012/04/26 09:01:59 pischky Exp $ 
#*******************************************************************************

# some of the following option may (or should) be overriden in main makefile ---

# the name of the application 
APPLICATION ?= Application

# the name used in comment of $(LOADER_FILE)
APPLICATION_NAME ?= $(APPLICATION)

# the application hex file without checksum 
APPLICATION_HEX ?= $(APPLICATION).hex

# the application hex file with checksum 
APPLICATION_WITH_CHECKSUM ?= $(APPLICATION)_checksum.hex

# flash file (contains complete flash: application + checksum + bootloader)
APPLICATION_WITH_BL ?= $(APPLICATION)_wBL.hex

# loader file (as required by JMRI: header + application + checksum)
LOADER_FILE ?= $(APPLICATION).dmf

# the hex file of the bootloader 
BOOTLOADER_HEX ?= Bootloader.hex

# ------------------------------------------------------------------------------

ifndef BOOTSTRAP_START_BYTE
    $(error BOOTSTRAP_START_BYTE is not define)
endif

ifndef UPDATE_OPTIONS
    $(error UPDATE_OPTIONS is not define)
endif

# The checksum is appended to the application. This line extracts the size
# of the application from the hex file and puts it into a variable
CHECKSUM_ADDRESS = $(strip $(shell avr-size -A $(APPLICATION_HEX) | grep Total | cut -b 6- ))

CHECKSUM_ADDRESS_MOD2 = $(shell let t1=$(CHECKSUM_ADDRESS); let "t2=$${t1}%2"; printf "%d\n" $${t2})

# The second checksum for flash file ( = BOOTSTRAP_START_BYTE - 2 )
# This lines subtract 2 from BOOTSTRAP_START_BYTE and formats as hex
# be carfull with comments in the definition of BOOTSTRAP_START_BYTE
CHECKSUM_ADDRESS_FLASH = $(shell let t1=$(BOOTSTRAP_START_BYTE); let "t2=$${t1}-2"; printf "0x%X\n" $${t2})

# evaluate UPDATE_OPTIONS
UPDATE_OPTIONS_2 = $(shell let t="$(UPDATE_OPTIONS)"; printf "%d\n" $${t})

# Behind the checksum we have to fill up to 6 zero bytes to make sure that
# the IPL protocol sends defined values (JMRI would fill undefined space
# with 0xFF) IPL sends the update in 8 byte chunks.
FILL_END = $(shell expr "(" "(" $(CHECKSUM_ADDRESS) + 8 ")" / 8 ")" "*" 8 )

# A simple addition checksum is embedded behind the application
# using the srecord package.
# (remove extended linear adress record with sed for digiIPL.exe 1.09) 
$(APPLICATION_WITH_CHECKSUM) : $(APPLICATION_HEX)
	@echo
	@echo $(MSG_GENFILE) $@
	@echo "CHECKSUM_ADDRESS      " = $(shell printf "0x%04X" $(CHECKSUM_ADDRESS)) = $(CHECKSUM_ADDRESS) 
	@echo "FILL_END              " = $(shell printf "0x%04X" $(FILL_END)) = $(FILL_END) 
	@echo "BOOTSTRAP_START_BYTE  " = $(shell printf "0x%04X" $(BOOTSTRAP_START_BYTE)) = $(shell printf "%d" $(BOOTSTRAP_START_BYTE))
	test $(CHECKSUM_ADDRESS_MOD2) -eq 0 # CHECKSUM_ADDRESS should be even
	#FIXME: errors from srec_cat, like missing input file, do not abort make
	srec_cat $(APPLICATION_HEX) -Intel \
	         -Little_Endian_Checksum_Negative $(CHECKSUM_ADDRESS) 2 2 \
	         -fill 0x00 0 $(FILL_END) \
	         -o - -Intel \
	| sed -e "/^:02000004/ D" >$(APPLICATION_WITH_CHECKSUM) # --nocr

# Generate hex file with application, checksum and bootloader. This
# file could be loaded into flash before first update by loconet.
# Unused program memory is filled by 0xFF to speed up the flashing.
# On flashing the complete flash memory is erased so the filled area
# is not stored in the output file. This filling requires a second checksum
# that is stored just below the bootloader. The extended linear adress
# record is removed from hex file.
$(APPLICATION_WITH_BL): $(APPLICATION_WITH_CHECKSUM) $(BOOTLOADER_HEX)
	@echo
	@echo $(MSG_GENFILE) $@
	@echo "CHECKSUM_ADDRESS_FLASH" = $(CHECKSUM_ADDRESS_FLASH)
	@echo "BOOTSTRAP_START_BYTE  " = $(BOOTSTRAP_START_BYTE)
	#FIXME: errors from srec_cat, like missing input file, do not abort make
	srec_cat $(APPLICATION_WITH_CHECKSUM) -Intel \
	         -fill 0xFF 0 $(CHECKSUM_ADDRESS_FLASH) \
	         -Little_Endian_Checksum_Negative $(CHECKSUM_ADDRESS_FLASH) 2 2 \
	         -unfill 0xFF 4 \
	         $(BOOTLOADER_HEX) -Intel \
	         -o - -Intel \
	| sed -e "/^:02000004/ D" >$(APPLICATION_WITH_BL) #  --nocr

# Create file that can be loaded into JMRI for software update.
# The hex file is prepended a header that is parsed by JMRI to
# identify the software in the IPL protocol in order to ensure
# that the right devices are updated.
$(LOADER_FILE): $(APPLICATION_WITH_CHECKSUM)
	@echo
	@echo $(MSG_GENFILE) $@
	echo -e -n "# Title / Description: generated for $(APPLICATION_NAME) on $(MCU)\r\n" > $(LOADER_FILE)
	echo -e -n "# Creation Date: $(shell date "+%Y-%m-%d %H:%M:%S %Z")\r\n" >> $(LOADER_FILE)
	echo -e -n "# Contents: Application plus checksum plus zero padding to 8 byte boundary\r\n" >> $(LOADER_FILE)
	echo -e -n "! Bootloader Version: 0\r\n"                 >> $(LOADER_FILE)
	echo -e -n "! Manufacturer Code: $(MANUFACTURER_ID)\r\n" >> $(LOADER_FILE)
	echo -e -n "! Developer Code: $(DEVELOPER_ID)\r\n"       >> $(LOADER_FILE)
	echo -e -n "! Product Code: $(PRODUCT_ID)\r\n"           >> $(LOADER_FILE)
	echo -e -n "! Hardware Version: $(HARDWARE_VERSION)\r\n" >> $(LOADER_FILE)
	echo -e -n "! Software Version: $(SOFTWARE_VERSION)\r\n" >> $(LOADER_FILE)
	echo -e -n "! Chunk Size: 64\r\n"                        >> $(LOADER_FILE)
	echo -e -n "! Delay: 25\r\n"                             >> $(LOADER_FILE)
	echo -e -n "! Options: $(UPDATE_OPTIONS_2)\r\n"          >> $(LOADER_FILE)
	echo -e -n "! EEPROM Start Address: F00000\r\n"          >> $(LOADER_FILE)
	cat $(APPLICATION_WITH_CHECKSUM) >> $(LOADER_FILE)

