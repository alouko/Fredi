/****************************************************************************
    Copyright (C) 2006, 2011 Martin Pischky, Olaf Funke

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    $Id: fuses.c,v 1.5 2012/04/27 10:31:03 pischky Exp $
******************************************************************************/

/*
 *  Define fuses in elf file
 */

#define LFUSE __attribute__ ((section ("lfuses")))
#define HFUSE __attribute__ ((section ("hfuses")))
#define EFUSE __attribute__ ((section ("efuses")))

//////////////////////////////////////////////////////////////////////////////////
#if defined(__AVR_ATmega8__)

  //
  //                  Fuse High Byte
  //
  // BOOTSZ1..0 = 10 (Boot Size: 256 words)
  // BOOTRST    = 0  (Reset Vector moved to the start of the Boot Flash Section)
  //
  #define BOOTRST     0  // ----------+
  #define BOOTSZ0     1  // ---------+|
  #define BOOTSZ1     2  // --------+||
  #define EESAVE      3  // -------+|||
  #define CKOPT       4  // -----+ ||||
  #define SPIEN       5  // ----+| ||||
  #define WDTON       6  // ---+|| ||||
  #define RSTDISBL    7  // --+||| ||||
  //                          |||| ||||
  //      hfuse:              1101 0100 = 0xD4

  unsigned char hfuse HFUSE =
    ( (1 << RSTDISBL ) | (1 << WDTON    ) | (0 << SPIEN    ) | (1 << CKOPT    )
    | (0 << EESAVE   ) | (1 << BOOTSZ1  ) | (0 << BOOTSZ0  ) | (0 << BOOTRST  ) );

  //
  //                  Fuse Low Byte
  //
  #define CKSEL0      0  // ----------+
  #define CKSEL1      1  // ---------+|
  #define CKSEL2      2  // --------+||
  #define CKSEL3      3  // -------+|||
  #define SUT0        4  // -----+ ||||
  #define SUT1        5  // ----+| ||||
  #define BODEN       6  // ---+|| ||||
  #define BODLEVEL    7  // --+||| ||||
  //                          |||| ||||
  //      lfuse:              1011 1111 = 0xBF

  unsigned char lfuse LFUSE =
    ( (1 << BODLEVEL ) | (0 << BODEN    ) | (1 << SUT1     ) | (1 << SUT0     )
    | (1 << CKSEL3   ) | (1 << CKSEL2   ) | (1 << CKSEL1   ) | (1 << CKSEL0   ) );

//////////////////////////////////////////////////////////////////////////////////
#elif defined(__AVR_ATmega88__)  | defined(__AVR_ATmega88A__)  \
    | defined(__AVR_ATmega88P__) | defined(__AVR_ATmega88PA__) \
    | defined(__AVR_ATmega168__) | defined(__AVR_ATmega168A__) \
    | defined(__AVR_ATmega168P__)| defined(__AVR_ATmega168PA__)

  #define X 0  // avrdude likes unused bits as "0",
               // Atmels opinion is "1" (but stk500.exe also accepts 0)

  //
  //                  Fuse Extended Byte
  //
  #define BOOTRST     0  // ----------+
  #define BOOTSZ0     1  // ---------+|
  #define BOOTSZ1     2  // --------+||
  #define UNUSED3     3  // -------+|||
  #define UNUSED4     4  // -----+ ||||
  #define UNUSED5     5  // ----+| ||||
  #define UNUSED6     6  // ---+|| ||||
  #define UNUSED7     7  // --+||| ||||
  //                          |||| ||||
  //      efuse:              XXXX X010 = 0xFA/0x02

  unsigned char efuse EFUSE =
    ( (X << UNUSED7  ) | (X << UNUSED6  ) | (X << UNUSED5  ) | (X << UNUSED4  )
    | (X << UNUSED3  ) | (0 << BOOTSZ1  ) | (1 << BOOTSZ0  ) | (0 << BOOTRST  ) );

  //
  //                  Fuse High Byte
  //
  #define BODLEVEL0   0  // ----------+
  #define BODLEVEL1   1  // ---------+|
  #define BODLEVEL2   2  // --------+||
  #define EESAVE      3  // -------+|||
  #define WDTON       4  // -----+ ||||
  #define SPIEN       5  // ----+| ||||
  #define DWEN        6  // ---+|| ||||
  #define RSTDISBL    7  // --+||| ||||
  //                          |||| ||||
  //      hfuse:              1101 0101 = 0xD5

  unsigned char hfuse HFUSE =
    ( (1 << RSTDISBL ) | (1 << DWEN     ) | (0 << SPIEN    ) | (1 << WDTON    )
    | (0 << EESAVE   ) | (1 << BODLEVEL2) | (0 << BODLEVEL1) | (1 << BODLEVEL0) );

  //
  //                  Fuse Low Byte
  //
  #define CKSEL0      0  // ----------+
  #define CKSEL1      1  // ---------+|
  #define CKSEL2      2  // --------+||
  #define CKSEL3      3  // -------+|||
  #define SUT0        4  // -----+ ||||
  #define SUT1        5  // ----+| ||||
  #define CKOUT       6  // ---+|| ||||
  #define CKDIV8      7  // --+||| ||||
  //                          |||| ||||
  //      lfuse:              1111 1101 = 0xFD

  //TODO
  unsigned char lfuse LFUSE =
    ( (1 << CKDIV8   ) | (1 << CKOUT    ) | (1 << SUT1     ) | (1 << SUT0     )
    | (1 << CKSEL3   ) | (1 << CKSEL2   ) | (0 << CKSEL1   ) | (1 << CKSEL0   ) );

//////////////////////////////////////////////////////////////////////////////////
#elif defined(__AVR_ATmega328__) | defined(__AVR_ATmega328P__) 

  #define X 0  // avrdude likes unused bits as "0",
               // Atmels opinion is "1" (but stk500.exe also accepts 0)

  //
  //                  Fuse Extended Byte
  //
  // BODLEVEL2..0 = 101 (V_BOT=2.7V)
  //
  #define BODLEVEL0   0  // ----------+
  #define BODLEVEL1   1  // ---------+|
  #define BODLEVEL2   2  // --------+||
  #define UNUSED3     3  // -------+|||
  #define UNUSED4     4  // -----+ ||||
  #define UNUSED5     5  // ----+| ||||
  #define UNUSED6     6  // ---+|| ||||
  #define UNUSED7     7  // --+||| ||||
  //                          |||| ||||
  //      efuse:              XXXX X101 = 0xFD/0x05

  unsigned char efuse EFUSE =
    ( (X << UNUSED7  ) | (X << UNUSED6  ) | (X << UNUSED5  ) | (X << UNUSED4  )
    | (X << UNUSED3  ) | (1 << BODLEVEL2) | (0 << BODLEVEL1) | (1 << BODLEVEL0) );

  //
  //                   Fuse High Byte
  //
  // RSTDISBL   = 1  (External Reset Enabled)
  // DWEN       = 1  (debugWIRE Disabled)
  // SPIEN      = 0  (Enable Serial Program and Data Downloading)
  // WDTON      = 1  (Watchdog Timer _is_not_ Always On)
  // EESAVE     = 0  (EEPROM memory is preserved through the Chip Erase)
  // BOOTSZ1..0 = 10 (Boot Size: 512 words)
  // BOOTRST    = 0  (Reset Vector moved to the start of the Boot Flash Section)
  //
  #define BOOTRST     0  // ----------+
  #define BOOTSZ0     1  // ---------+|
  #define BOOTSZ1     2  // --------+||
  #define EESAVE      3  // -------+|||
  #define WDTON       4  // -----+ ||||
  #define SPIEN       5  // ----+| ||||
  #define DWEN        6  // ---+|| ||||
  #define RSTDISBL    7  // --+||| ||||
  //                          |||| ||||
  //      hfuse:              1101 0100 = 0xD4

  unsigned char hfuse HFUSE =
    ( (1 << RSTDISBL ) | (1 << DWEN     ) | (0 << SPIEN    ) | (1 << WDTON    )
    | (0 << EESAVE   ) | (1 << BOOTSZ1  ) | (0 << BOOTSZ0  ) | (0 << BOOTRST  ) );

  //
  //                  Fuse Low Byte
  //
  // CKDIV8    = 1   (Do not divide clock by 8)
  // CKOUT     = 1   (No clock output to PORTB0)
  // ---- Low Power Crystal Oscillator --------------------------------------
  // CKSEL3..1 = 110 (3.0 - 8 MHz)  (see datasheet 8271D-AVR-05/11 Table 9-3)
  // CKSEL0    = 1   (Crystal Osc., BOD enabled / Start-up Time: 16K CK
  // SUT1..0   = 01  / Add. Delay from Reset: 14CK )
  //                                (see datasheet 8271D-AVR-05/11 Table 9-4)
  // ---- Full Swing Crystal Oscillator -------------------------------------
  // CKSEL3..1 = 011 (0.4 - 20 MHz) (see datasheet 8271D-AVR-05/11 Table 9-5)
  // CKSEL0    = 1   (Crystal Oscillator, BOD enabled / Start-up Time: 16K CK
  // SUT1..0   = 01  / Add. Delay from Reset: 14CK )
  //                                (see datasheet 8271D-AVR-05/11 Table 9-6)
  // ------------------------------------------------------------------------
  //
  #define CKSEL0      0  // ----------+
  #define CKSEL1      1  // ---------+|
  #define CKSEL2      2  // --------+||
  #define CKSEL3      3  // -------+|||
  #define SUT0        4  // -----+ ||||
  #define SUT1        5  // ----+| ||||
  #define CKOUT       6  // ---+|| ||||
  #define CKDIV8      7  // --+||| ||||
  //                          |||| ||||
  //      lfuse:              1101 1101 = 0xDD - Low Power Crystal Oscillator
  //      lfuse:              1101 0111 = 0xD7 - Full Swing Crystal Oscillator

  //TODO: test Low Power Crystal Oscillator
  unsigned char lfuse LFUSE =
    ( (1 << CKDIV8   ) | (1 << CKOUT    ) | (0 << SUT1     ) | (1 << SUT0     )
    | (0 << CKSEL3   ) | (1 << CKSEL2   ) | (1 << CKSEL1   ) | (1 << CKSEL0   ) );

//////////////////////////////////////////////////////////////////////////////////
#else

    #error unsupported MCU value (for now)

#endif
