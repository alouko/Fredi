;****************************************************************************
;    Copyright (C) 2006 Stefan Bormann
;
;    This library is free software; you can redistribute it and/or
;    modify it under the terms of the GNU Lesser General Public
;    License as published by the Free Software Foundation; either
;    version 2.1 of the License, or (at your option) any later version.
;
;    This library is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;    Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public
;    License along with this library; if not, write to the Free Software
;    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;
;*****************************************************************************
;
;    IMPORTANT:
;
;    Note: The sale any LocoNet device hardware (including bare PCB's) that
;    uses this or any other LocoNet software, requires testing and certification
;    by Digitrax Inc. and will be subject to a licensing agreement.
;
;    Please contact Digitrax Inc. for details.
;
;*****************************************************************************
;
; Title :   Bootloader definitions
; Author:   Stefan Bormann <stefan.bormann@gmx.de>
;           Martin Pischky <martin@pischky.de>
; Date:     7-May-2007, 5-Feb-2012
; Software: Atmel Assembler 2
; Target:   AtMega
;
; DESCRIPTION
;       Definitions of constands, register usage and common macros.
;
;
; $Id: BootloaderDef.inc,v 1.3 2012/02/10 19:49:49 pischky Exp $
;
;*****************************************************************************


;;;;;;;;;;;;;;;;;;; let's see the real values we use here ;;;;;;;;;;;;;;;;;;;;
#define STR2(x) #x
#define STR(x) STR2(x)
#define SHOWVAR(x, y) .warning STR(x ## y)

#message "MANUFACTURER_ID =" MANUFACTURER_ID
SHOWVAR("MANUFACTURER_ID =", MANUFACTURER_ID)
#message "DEVELOPER_ID =" DEVELOPER_ID
SHOWVAR("DEVELOPER_ID =", DEVELOPER_ID)
#message "PRODUCT_ID =" PRODUCT_ID
SHOWVAR("PRODUCT_ID =", PRODUCT_ID)
#if defined(__AVR_ATmega8__)
    #message "MCU = atmega8"
    .warning "MCU = atmega8"
#elif defined(__AVR_ATmega48__)
    #message "MCU = atmega48"
    .warning "MCU = atmega48"
#elif defined(__AVR_ATmega88__)
    #message "MCU = atmega88"
    .warning "MCU = atmega88"
#elif defined(__AVR_ATmega168__)
    #message "MCU = atmega168"
    .warning "MCU = atmega168"
#elif defined(__AVR_ATmega328P__)
    #message "MCU = atmega328p"
    .warning "MCU = atmega328p"
#else
    #message "MCU = <unknown>"
    .error "MCU = <unknown>"
#endif
#message "F_CPU =" F_CPU
SHOWVAR("F_CPU =", F_CPU)
#message "BOOTSTRAP_START_BYTE =" BOOTSTRAP_START_BYTE
SHOWVAR("BOOTSTRAP_START_BYTE =", BOOTSTRAP_START_BYTE)
#message "HARDWARE_VERS_TEST =" HARDWARE_VERS_TEST
SHOWVAR("HARDWARE_VERS_TEST =", HARDWARE_VERS_TEST)
#message "HARDWARE_VERSION =" HARDWARE_VERSION
SHOWVAR("HARDWARE_VERSION =", HARDWARE_VERSION)


;;;;;;;;;;;;;;;;;;; Constant definitions and register usage ;;;;;;;;;;;;;;;;;;;;;;


#ifndef BOOTSTRAP_START_BYTE
#error "please define BOOTSTRAP_START_BYTE with command line"
#endif

#define BOOTSTRAP_START_WORD (BOOTSTRAP_START_BYTE/2)

; definition of SPM_PAGESIZE depends on assembler used 
; iom8.h of avr-gcc defines it as bytes, m8def.inc of atmel defines it as words
; we need definition as bytes:
#define SPM_PAGESIZE PAGESIZE

#define ADDRESS_LOW  r30
#define ADDRESS_HIGH r31


#ifndef __AVR_ATmega8__
#define MCUCSR MCUSR
#define SPMCR SPMCSR
#define WDTCR WDTCSR
#define TIFR TIFR1
#endif

.macro StoreRegister dest, val
.if \dest < 0x40
	out   \dest, \val
.else
	sts   \dest, \val
.endif
.endm

.macro LoadRegister dest, prt
.if \prt < 0x40
	in    \dest, \prt
.else
	lds   \dest, \prt
.endif
.endm
