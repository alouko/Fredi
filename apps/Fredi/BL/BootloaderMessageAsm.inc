;****************************************************************************
;    Copyright (C) 2006 Stefan Bormann
;
;    This library is free software; you can redistribute it and/or
;    modify it under the terms of the GNU Lesser General Public
;    License as published by the Free Software Foundation; either
;    version 2.1 of the License, or (at your option) any later version.
;
;    This library is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;    Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public
;    License along with this library; if not, write to the Free Software
;    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;
;*****************************************************************************
;
;    IMPORTANT:
;
;    Note: The sale any LocoNet device hardware (including bare PCB's) that
;    uses this or any other LocoNet software, requires testing and certification
;    by Digitrax Inc. and will be subject to a licensing agreement.
;
;    Please contact Digitrax Inc. for details.
;
;*****************************************************************************
;
; Title :   Parser for IPL LocoNet Message
; Author:   Stefan Bormann <stefan.bormann@gmx.de>
; Date:     5-May-2006
; Software: Atmel Assembler 2
; Target:   ATmega
;
; DESCRIPTION
;       This module uses the RX only LN routines to receive an
;       OPC_PEER_XFER message as used by the IPL protocol.
;       The checksum over the downloaded area is included here, too.
;
;*****************************************************************************


.MACRO BootloaderMessageCheckSum
    clr   r24                 ;// in return register
    clr   r25                 ;unsigned short usChecksum = 0;

    clr   ADDRESS_LOW         ;unsigned short usAddress = 0;
    clr   ADDRESS_HIGH

BootloaderMessageCheckSumLoop:;do
    lpm   r20, Z+             ;{
    lpm   r21, Z+
    add   r24, r20            ;  usChecksum += pgm_read_word_near(usAddress+=2);	
    adc   r25, r21            ;}
                              ;while (usAddress!=BOOTSTRAP_START_BYTE);
    cpi   r30, lo8(BOOTSTRAP_START_BYTE)
    brne  BootloaderMessageCheckSumLoop
    cpi   r31, hi8(BOOTSTRAP_START_BYTE)
    brne  BootloaderMessageCheckSumLoop
.ENDM                     ;return usChecksum;



.MACRO P_UPPER_BITS           ;static void UpperBits(unsigned char *pucFirst)
    push  r15                 ;{
    push  r27                 ;  unsigned char ucData;

    ldi   r24, 0x04	          ;  unsigned char ucLoop=4;
    mov   r15, r24

    rcall LnRxOnlyData        ;  unsigned char ucPXCT=LnRxOnlyData();
    mov   r27, r24

    andi  r24, 0xF0	          ;  ucType = ucPXCT & 0xF0;
    sts   ucType, r24
                              ;  do
UpperBitsLoop:                ;  {
    rcall LnRxOnlyData        ;    ucData = LnRxOnlyData();

    sbrc  r27, 0              ;    if (ucPXCT & 1) ucData |= 0x80;
    ori   r24, 0x80

    st    Y+, r24             ;    *(pucFirst++) = ucData;

    lsr   r27                 ;    ucPXCT >>= 1;

    dec   r15                 ;    ucLoop--;
    brne  UpperBitsLoop       ;  }
                              ;  while (ucLoop);
    pop   r27
    pop   r15

    lds   r24, ucType         ;  return ucType
    ret
.ENDM                         ;}



;// 0xE5 0x10 0xFF 0xFF 0xFF PXCT1 D1 D2 D3 D4 PXCT2 D5 D6 D7 D8 
.macro BootloaderMessageReceive p0 
                               ; void BootloaderMessageReceive(void)
                               ;{
    LnRxOnlyOpcode    	       ;if (LnRxOnlyOpcode() != OPC_PEER_XFER) return;
    cpi   r24, 0xE5	
    brne  \p0

    rcall LnRxOnlyData         ;if (LnRxOnlyData() != 0x10) return;
    cpi   r24, 0x10	
    brne  \p0

    ldi   r28, 3               ;r28 = 3
MessageCheck7fLoop:            ;do
    rcall LnRxOnlyData         ;{
    cpi   r24, 0x7F	           ;  if (LnRxOnlyData() != 0x7F) return;
    brne  \p0                  ;}

    dec   r28                  ;while (--r28);
    brne  MessageCheck7fLoop

    ldi   r28, lo8(aucData)    ;UpperBits(&aucData[0]);
    ldi   r29, hi8(aucData)
    rcall UpperBits

    cpi   r24, 0x40	           ;if (ucType != 0x40) return;
    brne  \p0

    rcall UpperBits            ;UpperBits(&aucData[4]);

    swap  r24                  ;ucType >>= 4;
    andi  r24, 0x0F
    sts   ucType, r24

    LnRxOnlyChecksum           ;LnRxOnlyChecksum();
.ENDM
