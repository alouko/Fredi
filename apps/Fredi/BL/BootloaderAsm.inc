;****************************************************************************
;    Copyright (C) 2006 Stefan Bormann
;
;    This library is free software; you can redistribute it and/or
;    modify it under the terms of the GNU Lesser General Public
;    License as published by the Free Software Foundation; either
;    version 2.1 of the License, or (at your option) any later version.
;
;    This library is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;    Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public
;    License along with this library; if not, write to the Free Software
;    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;
;*****************************************************************************
;
;    IMPORTANT:
;
;    Note: The sale any LocoNet device hardware (including bare PCB's) that
;    uses this or any other LocoNet software, requires testing and certification
;    by Digitrax Inc. and will be subject to a licensing agreement.
;
;    Please contact Digitrax Inc. for details.
;
;*****************************************************************************
;
; Title :   Bootloader main loop
; Author:   Stefan Bormann <stefan.bormann@gmx.de>
; Date:     5-May-2006
; Software: Atmel Assembler 2
; Target:   AtMega
;
; DESCRIPTION
;       Main module of bootstraploader, handles incoming LN packets.
;
;*****************************************************************************


;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Variable definition ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


.data
ucActive:	
	.byte 0
ucType:
	.byte 0
ucLnRxOnlyChecksum:
	.byte 0
aucData:	
	.byte 0,0,0,0, 0,0,0,0

; Data byte indices in setup message
.equ INDEX_MFG         , 0
.equ INDEX_PROD_LOW    , 1
.equ INDEX_HW_VERSION  , 2
.equ INDEX_SW_VERSION  , 3
.equ INDEX_FLAGS       , 4
.equ INDEX_DEVELOPER   , 6
.equ INDEX_PROD_HIGH   , 7

; Bit in flags byte
.equ FLAG_HW_CHECK     , 0
.equ FLAG_HW_HIGHER_OK , 1
.equ FLAG_SW_CHECK     , 2  ; not used in bootloader, because SW is not available for check

.equ BOOTLOADER_VERSION , 1 ; not used at the moment, but might be useful in the future

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Reset vector ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.text


#ifdef HARDWARE_VERSION
    #define HARDWARE_VERSION_FLASH HARDWARE_VERSION
#else
    #define HARDWARE_VERSION_FLASH 0xff
#endif


;;.org  BOOTSTRAP_START_WORD  /* reset vector is entry of bootloader ist forst word of bootloader */

;------ Initialize stack
    ldi   r28, lo8(RAMEND);
    ldi   r29, hi8(RAMEND);
    out   SPH, r29;
    out   SPL, r28;

    clr   r1;   /* convention from GCC, inherited for convenience */

    DebugInit  /* initialize port for debug output */

    in    r24, MCUCSR         ; // Sample here, evaluate behind watchdog disable
                              ; stored in r24 --->
  ; needed for mega168, this sucks
#ifdef RESET_WATCHDOG_ON_STARTUP
    out   MCUCSR,r1   ; reset WDRF
    ldi   r25, ((1<<WDCE) | (1<<WDE))
    StoreRegister  WDTCSR, r25
    StoreRegister  WDTCSR, r1
#endif
/* Initialize any other hardware */
    InitHardware

;------ Decide cause of entry: call from application or reset 
                              ; read from r24 <----
    and   r24, r24            ;if (MCUCSR==0)  // if no reset reason available, 
    brne  EntryByReset        ;{               // than we just where called from the application
    ldi   r25, 0x01           ;   ucActive = 1;
    DebugOutputFromApp        ;   printf("app called bootloader");  // entering bootloader from application
    rjmp  StartupPort         ;}
                              ;else
EntryByReset:                 ;{
    BootloaderMessageCheckSum ;   if (BootloaderMessageCheckSum() == 0)  // if check ok, than jump to application
    or    r24, r25            ;
    brne  StartupFromReset    ;
    DebugOutputToApp          ;   {
    clr   r30                 ;      printf("booting to app"); // reset -> application
    clr   r31                 ;      jump_to_application();  
    ijmp                      ;   }

StartupFromReset:
    DebugOutputInvalid        ;   printf("invalid app");  // entering bootloader from reset
    clr   r25                 ;   ucActive = 0;  // need setup message to start update

StartupPort:
    sts   ucActive, r25       ;}

    cli                       ;cli();

    LnRxOnlyInit              ;LnRxOnlyInit();
    InitLocoNetHardware       ;InitLocoNetHardware();  //Init Hardware (e.g. Analog Comparator on FREDI) 

StartMainLoop:
    clr   ADDRESS_LOW         ;usAddress = 0;
    clr   ADDRESS_HIGH

MainLoop:                     ;for (;;)
                              ;{
    BootloaderMessageReceive MainLoop
; if (!BootloaderMessageReceive()) continue;

    lds   r25, ucType         ;  if (ucType==0)  // setup
    and   r25, r25
    brne  MainLoopNotStart    ;  {

    ldi   r28, lo8(aucData)   ;    unsigned short *pusData = &aucData[0];
    ldi   r29, hi8(aucData)  ;    // possibly should we reuse this pointer from BootlloaderMessageReceive()????

    ld    r24, Y+             ;
    cpi   r24, MANUFACTURER_ID;    if (MANUFACTURER_ID != *pusData++)
    brne  MainLoop            ;      continue;

    ld    r24, Y+
    cpi   r24, lo8(PRODUCT_ID);    if ((low_byte(PRODUCT_ID) != *pusData++)
    brne  MainLoop            ;      continue

    ld    r25, Y+             ;    unsigned char ucHwVer = *pusData++;  

#ifdef HARDWARE_VERSION //; does this project uses HW versions?
    #if ! defined(HARDWARE_VERS_TEST)
        #error Please define HARDWARE_VERS_TEST in your makefile and pass to compiler with -D
    #elif (HARDWARE_VERS_TEST != 1) && (HARDWARE_VERS_TEST != 2)
        #error HARDWARE_VERS_TEST should be defined as '1' or '2'
    #else
        #if HARDWARE_VERS_TEST == 1

    adiw  r28, 1              ;    pusData++; // skipping SW-version
    ld    r24, Y+             ;    ucFlags = *pusData++
    sbrs  r24, FLAG_HW_CHECK  ;    if (ucFlags & 0x01)
    rjmp  SetupEndOfHwCheck   ;    {
    cpi   r25, HARDWARE_VERSION;     if (ucHwVer!=HARDWARE_VERSION)
    breq  SetupEndOfHwCheck   ;      {
    sbrs  r24, FLAG_HW_HIGHER_OK;      if ((ucFlags & 0x02)==0) continue;
    rjmp  MainLoop            ;        
    brsh  AnotherJumpToMainLoop;       if (ucHwVer>=HARDWARE_VERSION) continue;
                              ;      }
                              ;    }

        #elif HARDWARE_VERS_TEST == 2
            .warning "compiling with forced exact hardware version match"

    adiw  r28, 2              ;    pusData += 2; // skipping SW-version and Flags
    cpi   r25, HARDWARE_VERSION;   if (ucHwVer!=HARDWARE_VERSION)
    breq  SetupEndOfHwCheck   ;    {
    rjmp  MainLoop            ;      continue;
                              ;    }

        #endif
    #endif
SetupEndOfHwCheck:


    adiw  r28, 1              ;    pusData++; // skipping spare byte

#else

    adiw  r28, 3              ;    pusData += 3;  // skipping SW-version, Flags and spare byte

#endif

    ld    r24, Y+             ;
    cpi   r24, DEVELOPER_ID   ;    if ((DEVELOPER_ID != aucData[6])
    brne  MainLoop            ;      continue

    ld    r24, Y+             ;
    cpi   r24, hi8(PRODUCT_ID);   if ((high_byte(PRODUCT_ID) != aucData[7])
    brne  AnotherJumpToMainLoop;     continue

    ldi   r24, 0x01
    sts   ucActive, r24       ;    ucActive = 1;
    rjmp  StartMainLoop       ;    usAddress = 0;
                              ;  }

MainLoopNotStart:             ;  else
    lds   r24, ucActive
    and   r24, r24            ;  if (ucActive)
    breq  AnotherJumpToMainLoop; {

MainLoopIsActive:             ;    if (ucType==1) // set address
    cpi   r25, 0x01           ;    {
    brne  MainLoopNotSetAddress

    lds   r25, aucData+1
    lds   r24, aucData+2
    cp    r24, ADDRESS_LOW    ;      if ((aucData[2] != lowbyte (usAddress)) ||
    cpc   r25, ADDRESS_HIGH   ;          (aucData[1] != highbyte(usAddress)) ||
    brne  SetAddressAbort
    lds   r24, aucData+0
    cpse  r24, r1             ;          (aucData[0] != 0))
SetAddressAbort:              ;      {
    rcall LnRxOnlySevereError;         LnRxOnlySevereError();
AnotherJumpToMainLoop:        ;      } 
    rjmp  MainLoop            ;    }

MainLoopNotSetAddress:        ;  
    cpi   r25, 0x02           ;    else if (ucType==2) // transfer data
    brne  MainLoopNotTransferData
                              ;    {
    rcall TransferData        ;      TransferData()
    rjmp  MainLoop            ;    }

MainLoopNotTransferData:      ;    else if (ucType==4) // end operation
    cpi   r25, 0x04           ;    {
    brne  AnotherJumpToMainLoop;     EndOperation();
                              ;    }
                              ;  }//if(ucActive)
                              ;}//for()

DoEndOperation:               ;void EndOperation(void)
    ldi   r28, lo8(aucData)
    ldi   r29, hi8(aucData)  ; Y = &aucData;
    ldi   r24, 8              ; ucLoop = 8;
                              ; while (ucLoop)
MemFillDataLoop:              ; {
    st    Y+, r1              ;   *(Y++) = 0;
    dec   r24                 ;   ucLoop--;
    brne  MemFillDataLoop     ; }
    
FlashFillLoop:                ;for (;;)
    rcall TransferData        ;  TransferData()
    rjmp  FlashFillLoop       ;// aborting loop with reset when TransferData() determines end of flash


TransferData:                 ;void TransferData(void)
                              ;{
    Burn8Bytes                ;  Burn8Bytes();

    movw  r24, ADDRESS_LOW    ;  unsigned short usTemp = usAddress;
    andi  r24, lo8(SPM_PAGESIZE-1)
    andi  r25, hi8(SPM_PAGESIZE-1)
    or    r24, r25            ;  if ((usAddress & (SPM_PAGESIZE-1)) == 0)
    brne  TransferDataExit    ;  {
    rcall BurnPage            ;    BurnPage();

    cpi   ADDRESS_LOW,  lo8 (BOOTSTRAP_START_BYTE) ;//reached end of flashable area?
    brne  TransferDataExit    ;    if (usAddress == BOOTSTRAP_START_BYTE)
    cpi   ADDRESS_HIGH, hi8(BOOTSTRAP_START_BYTE)
    breq  LnRxOnlySevereError ;      LnRxOnlySevereError(); //reset
TransferDataExit:             ;  }
    ret                       ;}



LnRxOnlySevereError:          ;  // other sw modules call directly to this address
    P_LN_RX_ONLY_SEVERE_ERROR ;  LnRxOnlySevereError();  // download complete -> reboot
                              ;}

UpperBits:
    P_UPPER_BITS   ; function from this module instanciated here

BurnPage:
    P_BURN_PAGE

LnRxOnlyData:
    P_LN_RX_ONLY_DATA

.org  FLASHEND-BOOTSTRAP_START_BYTE-1, 0xff              /* last word of flash memory is last word of bootloader */
ConstTable:  .byte  HARDWARE_VERSION_FLASH, BOOTLOADER_VERSION
