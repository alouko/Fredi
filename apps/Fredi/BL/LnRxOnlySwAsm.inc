;****************************************************************************
;    Copyright (C) 2006 Stefan Bormann
;
;    This library is free software; you can redistribute it and/or
;    modify it under the terms of the GNU Lesser General Public
;    License as published by the Free Software Foundation; either
;    version 2.1 of the License, or (at your option) any later version.
;
;    This library is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;    Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public
;    License along with this library; if not, write to the Free Software
;    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;
;*****************************************************************************
;
;    IMPORTANT:
;
;    Note: The sale any LocoNet device hardware (including bare PCB's) that
;    uses this or any other LocoNet software, requires testing and certification
;    by Digitrax Inc. and will be subject to a licensing agreement.
;
;    Please contact Digitrax Inc. for details.
;
;*****************************************************************************
;
; Title :   LocoNet SW Receive Only
; Author:   Stefan Bormann <stefan.bormann@gmx.de>
;           (Martin Pischky <martin@pischky.de>)
; Date:     5-May-2006, 21-Apr-2012
; Software: Atmel Assembler 2
; Target:   AtMega
;
; DESCRIPTION
;       Basic receive only LocoNet access, uses bit banging.
;
; $Id: LnRxOnlySwAsm.inc,v 1.6 2012/04/21 11:58:12 pischky Exp $
;
;*****************************************************************************

; // used in P_LN_RX_ONLY_DATA
#define COMPARE_TARGET_LO r18 
#define COMPARE_TARGET_HI r19 
#define BIT_LOOP_COUNTER  r25 
; // used by SkipIfPortPinIsClear, SkipIfPortPinIsSet
#define TEMP_REG          r0  
;                              // which are used by P_LN_RX_ONLY_DATA
; // used in P_LN_RX_ONLY_DATA
#define TEMP_REG_HIGH     r26 
;// return value of P_LN_RX_ONLY_DATA
#define UCDATA_MIRROR     r24 


.macro LnRxOnlyInit
    ldi   r24, 0x01           ; TCCR1B = 0x01;
    StoreRegister TCCR1B,r24         ; // no prescaler, normal mode
.endm



.macro SkipIfPortPinIsClear p, b
    ;; replacement of SBIC instruction if port address is above 0x1f
    ;; parameter:   @0: the port name (here: LN_RX_PORT)
    ;;              @1: the bit number (here: LN_RX_BIT)
    .if \p < 0x20
        sbic  \p, \b              ; skip following instruction, if LocoNet is low
    .else
        LoadRegister TEMP_REG, \p ; load temp register with received data bit
        sbrc TEMP_REG, \b         ; skip following instruction, if pin is low
    .endif
.endm



.macro SkipIfPortPinIsSet p, b
    ;; replacement of SBIC instruction if port address is above 0x1f
    ;; parameter:   @0: the port name (here: LN_RX_PORT)
    ;;              @1: the bit number (here: LN_RX_BIT)
    .if \p < 0x20
        sbis  \p, \b              ; skip following instruction, if LocoNet is low
    .else
        LoadRegister TEMP_REG, \p ; load temp register with received data bit
        sbrs TEMP_REG, \b         ; skip following instruction, if pin is low
    .endif
.endm



.macro P_LN_RX_ONLY_SEVERE_ERROR
    ldi   r24, 0x18             ; This initializes the watchdog timer and waits
    ldi   r18, 0x08             ; for it to elapse in an endless loop.
    StoreRegister   WDTCSR, r24            ; This is the only way to trigger a hardware
    StoreRegister   WDTCSR, r18            ; reset from software.
LnRxOnlySevereErrorLoop:
    rjmp  LnRxOnlySevereErrorLoop
.endm



.macro LnRxOnlyChecksum       ;void LnRxOnlyChecksum(void)
                              ;{
    rcall LnRxOnlyData        ;  LnRxOnlyData();  // rx last byte of packet
    lds   r24, ucLnRxOnlyChecksum
    ldi   r25, 0xff           ;  if (ucLnRxOnlyChecksum!=0xff)
    cpse  r24, r25            ;  {
    rcall LnRxOnlySevereError ;    LnRxOnlySevereError();
                              ;  }
.endm                         ;}



.macro LnRxOnlyOpcode         ;unsigned char LnRxOnlyOpcode(void)
                              ;{
LnRxOnlyOpcodeWaitOpc:        ;  do
                              ;  {
    sts   ucLnRxOnlyChecksum, r1;  ucLnRxOnlyChecksum = 0;
                              
LnRxOnlyOpcodeWaitIdle:       ;    // wait for IDLE=1 state
                              ;    loop_until_bit_is_set(LN_RX_PORT, LN_RX_BIT);
    SkipIfPortPinIsSet LN_RX_PORT,LN_RX_BIT
    rjmp  LnRxOnlyOpcodeWaitIdle

    rcall LnRxOnlyData        ;    LnRxOnlyData();
                              ;  }  // loop if not opcode or byte framing error
    and   r24, r24            ;  while (bit_is_clear(ucData, 7));
    brge  LnRxOnlyOpcodeWaitOpc
                              ;  return ucData;  // r24
.endm                         ;}



;#define LN_TIMER_RX_START_PERIOD  720
;#define LN_TIMER_RX_RELOAD_PERIOD 480

#define LN_BIT_PERIOD       (F_CPU / 16666)
;          // The Start Bit period is a full bit period + half of the next bit period
;          // so that the bit is sampled in middle of the bit
#define LN_TIMER_RX_START_PERIOD    LN_BIT_PERIOD + (LN_BIT_PERIOD / 2)
#define LN_TIMER_RX_RELOAD_PERIOD   LN_BIT_PERIOD 


.macro P_LN_RX_ONLY_DATA      ;unsigned char LnRxOnlyData(void)
                              ;{
LnRxOnlyDataWaitStart:        ;  unsigned char ucLoop;
                              ; //--- wait for start bit
    SkipIfPortPinIsClear LN_RX_PORT,LN_RX_BIT 
    rjmp  LnRxOnlyDataWaitStart; loop_until_bit_is_clear(LN_RX_PORT, LN_RX_BIT);

    LoadRegister   COMPARE_TARGET_LO,TCNT1L;//--- Get the Current Timer1 Count and Add the offset
    LoadRegister   COMPARE_TARGET_HI,TCNT1H;//    for the Compare target to find the middle of 
                              ;  lnCompareTarget = TCNT1 + LN_TIMER_RX_START_PERIOD;
    subi  COMPARE_TARGET_LO,lo8(-(LN_TIMER_RX_START_PERIOD))
    sbci  COMPARE_TARGET_HI,hi8(-(LN_TIMER_RX_START_PERIOD))

    StoreRegister   OCR1AH,COMPARE_TARGET_HI;OCR1A = lnCompareTarget ;
    StoreRegister   OCR1AL,COMPARE_TARGET_LO

    ldi   BIT_LOOP_COUNTER,8  ;  for (ucLoop=0; ucLoop<8; ucLoop++)  
LnRxOnlyDataBitLoop:          ;  { //--- Loop over data bits
                              ;    //--- Waiting for data bit
    lsr   UCDATA_MIRROR       ;    ucData >>= 1;   // Loconet is LSB first, shifting data in from left

    in    TEMP_REG_HIGH,TIFR  ;    sbi(TIFR, OCF1A);
    ori   TEMP_REG_HIGH,(1<<OCF1A)
    out   TIFR,TEMP_REG_HIGH

LnRxOnlyDataWaitDataMiddle:   ;    // wait for middle of data bit
    in    TEMP_REG_HIGH,TIFR  ;    loop_until_bit_is_set(TIFR, OCF1A);
    sbrs  TEMP_REG_HIGH,OCF1A
    rjmp  LnRxOnlyDataWaitDataMiddle
                              ;    //--- Copy data bit from input to MSB of data buffer
                              ;    if (bit_is_set(LN_RX_PORT, LN_RX_BIT))
    SkipIfPortPinIsClear LN_RX_PORT,LN_RX_BIT
                              ;    {
    ori   UCDATA_MIRROR,lo8(0x80);   sbi(ucData, 7);
                              ;    }
                              ;    //--- Calculate timer target for next data (or stop) bit
                              ;    lnCompareTarget += LN_TIMER_RX_RELOAD_PERIOD;
    subi  COMPARE_TARGET_LO,lo8(-(LN_TIMER_RX_RELOAD_PERIOD))
    sbci  COMPARE_TARGET_HI,hi8(-(LN_TIMER_RX_RELOAD_PERIOD))
                              
    StoreRegister   OCR1AH,COMPARE_TARGET_HI;OCR1A = lnCompareTarget;
    StoreRegister   OCR1AL,COMPARE_TARGET_LO

    subi  BIT_LOOP_COUNTER,1
    brne  LnRxOnlyDataBitLoop ;  }

    in    TEMP_REG_HIGH,TIFR  ;  //--- Check stop bit
    ori   TEMP_REG_HIGH,(1<<OCF1A)
    out   TIFR,TEMP_REG_HIGH  ;  sbi(TIFR, OCF1A);

LnRxOnlyDataWaitStopMiddle:   
    in    TEMP_REG_HIGH,TIFR  ;  loop_until_bit_is_set(TIFR, OCF1A);  // wait for middle of stop bit
    sbrs  TEMP_REG_HIGH,OCF1A
    rjmp  LnRxOnlyDataWaitStopMiddle
                              ;  // check for stop bit to be 1
                              ;  if (bit_is_clear(LN_RX_PORT, LN_RX_BIT))
    SkipIfPortPinIsSet LN_RX_PORT,LN_RX_BIT
                              ;  {
    rcall LnRxOnlySevereError ;    LnRxOnlySevereError();
                              ;  }
                              ;  ucLnRxOnlyChecksum ^= ucData;
    lds   TEMP_REG_HIGH,ucLnRxOnlyChecksum
    eor   TEMP_REG_HIGH,UCDATA_MIRROR
    sts   ucLnRxOnlyChecksum,TEMP_REG_HIGH

    ret                       ;  return ucData;  //r24=UCDATA_MIRROR
.endm                         ;}
