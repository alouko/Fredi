;****************************************************************************
;    Copyright (C) 2006 Stefan Bormann
;
;    This library is free software; you can redistribute it and/or
;    modify it under the terms of the GNU Lesser General Public
;    License as published by the Free Software Foundation; either
;    version 2.1 of the License, or (at your option) any later version.
;
;    This library is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;    Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public
;    License along with this library; if not, write to the Free Software
;    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;
;*****************************************************************************
;
;    IMPORTANT:
;
;    Note: The sale any LocoNet device hardware (including bare PCB's) that
;    uses this or any other LocoNet software, requires testing and certification
;    by Digitrax Inc. and will be subject to a licensing agreement.
;
;    Please contact Digitrax Inc. for details.
;
;*****************************************************************************
;
; Title :   LocoNet SW Receive Only
; Author:   Stefan Bormann <stefan.bormann@gmx.de>
; Date:     5-May-2006
; Software: Atmel Assembler 2
; Target:   AtMega
;
; DESCRIPTION
;       Basic receive only LocoNet access, uses bit banging.
;
;*****************************************************************************


#define BAUD_RATE_REG ((F_CPU)/(16*16667)-1)

.MACRO LnRxOnlyInit
    ldi r24, BAUD_RATE_REG
    StoreRegister  UBRR0L, r24
    ldi r24, (1<<RXEN0)
    StoreRegister  UCSR0B, r24
.ENDMACRO



.MACRO P_LN_RX_ONLY_SEVERE_ERROR
    ldi   r24, 0x18             ; This initializes the watchdog timer and waits
    ldi   r18, 0x08             ; for it to elapse in an endless loop.
    StoreRegister   WDTCSR, r24            ; This is the only way to trigger a hardware
    StoreRegister   WDTCSR, r18            ; reset from software.
LnRxOnlySevereErrorLoop:
    rjmp  LnRxOnlySevereErrorLoop
.ENDMACRO



.MACRO LnRxOnlyChecksum       ;void LnRxOnlyChecksum(void)
                              ;{
    rcall LnRxOnlyData        ;  LnRxOnlyData();  // rx last byte of packet
    lds   r24, ucLnRxOnlyChecksum
    ldi   r25, 0xff           ;  if (ucLnRxOnlyChecksum!=0xff)
    cpse  r24, r25            ;  {
    rcall LnRxOnlySevereError ;    LnRxOnlySevereError();
                              ;  }
.ENDMACRO                     ;}



.MACRO LnRxOnlyOpcode         ;unsigned char LnRxOnlyOpcode(void)
                              ;{
LnRxOnlyOpcodeWaitOpc:        ;  do
                              ;  {
    sts   ucLnRxOnlyChecksum, r1;  ucLnRxOnlyChecksum = 0;
                              
    rcall LnRxOnlyData        ;    LnRxOnlyData();
                              ;  }  // loop if not opcode or byte framing error
    and   r24, r24            ;  while (bit_is_clear(ucData, 7));
    brge  LnRxOnlyOpcodeWaitOpc
                              ;  return ucData;  // r24
.ENDMACRO                     ;}



#define COMPARE_TARGET_LO r18
#define COMPARE_TARGET_HI r19
#define BIT_LOOP_COUNTER  r25
#define TEMP_REG          r0
#define TEMP_REG_HIGH     r26
#define UCDATA_MIRROR     r24  // return value


;#define LN_TIMER_RX_START_PERIOD  720
;#define LN_TIMER_RX_RELOAD_PERIOD 480

#define LN_BIT_PERIOD       (F_CPU / 16666)
          // The Start Bit period is a full bit period + half of the next bit period
          // so that the bit is sampled in middle of the bit
#define LN_TIMER_RX_START_PERIOD    LN_BIT_PERIOD + (LN_BIT_PERIOD / 2)
#define LN_TIMER_RX_RELOAD_PERIOD   LN_BIT_PERIOD 


.MACRO P_LN_RX_ONLY_DATA      ;unsigned char LnRxOnlyData(void)
                              ;{
LnRxOnlyDataLoop:
    lds   UCDATA_MIRROR, UCSR0A
    sbrs  UCDATA_MIRROR, RXC0        ;  while (!RXC0)
    rjmp  LnRxOnlyDataLoop    ;  {}
    lds   UCDATA_MIRROR, UDR0 ;  ucData = UDR;  // receive byte
                              ;  ucLnRxOnlyChecksum ^= ucData;
    lds   TEMP_REG_HIGH,ucLnRxOnlyChecksum
    eor   TEMP_REG_HIGH,UCDATA_MIRROR
    sts   ucLnRxOnlyChecksum,TEMP_REG_HIGH

    ret                       ;  return ucData;  //r24=UCDATA_MIRROR
.ENDMACRO                     ;}
