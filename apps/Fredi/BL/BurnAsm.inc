;****************************************************************************
;    Copyright (C) 2006 Stefan Bormann
;
;    This library is free software; you can redistribute it and/or
;    modify it under the terms of the GNU Lesser General Public
;    License as published by the Free Software Foundation; either
;    version 2.1 of the License, or (at your option) any later version.
;
;    This library is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;    Lesser General Public License for more details.
;
;    You should have received a copy of the GNU Lesser General Public
;    License along with this library; if not, write to the Free Software
;    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;
;*****************************************************************************
;
;    IMPORTANT:
;
;    Note: The sale any LocoNet device hardware (including bare PCB's) that
;    uses this or any other LocoNet software, requires testing and certification
;    by Digitrax Inc. and will be subject to a licensing agreement.
;
;    Please contact Digitrax Inc. for details.
;
;*****************************************************************************
;
; Title :   Write access for on chip program flash memory
; Author:   Stefan Bormann <stefan.bormann@gmx.de>
; Date:     5-May-2006
; Software: Atmel Assembler 2
; Target:   ATmega
;
; DESCRIPTION
;       This module implements two macrons, one copies eight bytes into
;       the hardware buffer for flash write preparation and another that
;       erases a flash page and than burns it with the new prepared code.
;
;*****************************************************************************


.MACRO Burn8Bytes
    ldi   r20, 0x04           ;unsigned char ucLoop=4;

    ldi   r26, lo8(aucData)   ;unsigned short *pusSource = (unsigned short *)&aucData[0];
    ldi   r27, hi8(aucData)
                              ;do
loopBurn8Bytes:               ;{
    ld    r0, X+              ;  boot_page_fill(usAddress, *pusSource++);
    ld    r1, X+              
    ldi   r21, 0x01
    out   SPMCR, r21
    spm
    eor   r1, r1

    adiw  r30, 2              ;  usAddress += 2;

    subi  r20, 0x01           ;  ucLoop--;

    brne  loopBurn8Bytes      ;}
.ENDM                         ;while(ucLoop);


;; Attention: >>8 and /256 are not identical. Think about sign!!
.equ PAGE_MASK,  (~(SPM_PAGESIZE-1))
.equ PAGE_MASKhi, (((~(SPM_PAGESIZE-1))>>8) & 0xff)


.MACRO P_BURN_PAGE
    movw  r28, ADDRESS_LOW    ;// backup
    sbiw  ADDRESS_LOW, 1      ;unsigned short usPage = (usAddress-1) & (~(SPM_PAGESIZE-1));
    andi  ADDRESS_LOW, lo8(PAGE_MASK) 
.if  PAGE_MASKhi != 0xff
    andi  ADDRESS_HIGH, hi8(PAGE_MASK)
.endif

    DebugOutputBurn           ;ADDRESS_LOW:ADDRESS_HIGH just passed a page boundary

    ldi   r24, 0x03           ;boot_page_erase (usPage);
    rcall StoreProgramMemory

    ldi   r24, 0x05           ;boot_page_write (usPage);     // Store buffer in flash page.
    rcall StoreProgramMemory

    movw  ADDRESS_LOW, r28    ;//restore
    ret


StoreProgramMemory:
    out   SPMCR, r24
    spm

StoreProgramMemoryWait:       ;// Wait until the memory is erased.
    in    r0, SPMCR           ;boot_spm_busy_wait ();
    sbrc  r0, 0
    rjmp  StoreProgramMemoryWait
    ret
.ENDM
